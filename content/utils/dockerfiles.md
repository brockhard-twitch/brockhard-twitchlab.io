---
title: Dockerfiles
weight: 5
---
## Angular

### _Dockerfile_
```
FROM node:latest as dev
WORKDIR /work
RUN npm i -g @angular/cli
COPY package*.json .
RUN npm ci
COPY . .
EXPOSE 4200
CMD ["ng","serve","--host","0.0.0.0","--port","4200"]

FROM node:latest as builder
WORKDIR /build
COPY --from=dev /work .
RUN ng build

FROM lscr.io/linuxserver/nginx:latest as prod
ENV PUID=1000
ENV PGID=1000
ENV TZ=America/Phoenix
EXPOSE 443
EXPOSE 80
COPY --from=builder /build/dist/<appname> /config/www
```

### _docker-compose.yml_ 
```
version: '3'

services:
  dev:
    container_name: dev
    image: ang-dev
    build:
      context: .
      target: dev
    ports:
      - 4200:4200
    volumes:
      - ".:/work"
    command: ["ng","serve","--host","0.0.0.0","--port","4200"]
  
  prod:
    container_name: prod-test
    image: ang-prod-test
    build:
      context: .
      target: prod
    ports:
      - 443:443
      - 80:80
```