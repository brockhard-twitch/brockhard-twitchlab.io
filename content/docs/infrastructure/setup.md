---
title: Gitlab Repo Setup
weight: 10
description: > 
  "terraform"
---

To Correctly setup the pipeline you need to ser the environment variables in
: Settings > CI/CD > variables

{{< tip >}}
Adding a prefix of _TF_VAR__ will make the variable in CICD accessable as an environment variable in terraform
{{< /tip >}}

## Variables Needed
--- 
- TF_VAR_MANTIS_ZONE_ID
- TF_VAR_CLOUDFLARE_API_KEY
- TF_VAR_PIHOLE_PASSWORD

## PG Connection String
**_conn_str_**
: postgres://terraform:M@ntisdCloudPg@10.10.1.3:5432/terraform_backend?sslmode=disable

