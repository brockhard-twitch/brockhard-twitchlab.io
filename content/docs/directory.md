---
title: "Servers Directory"
weight: 2
description: >
  Mantisd Server info 
---

|Name|URL|IP|Ports|
|---|---|---|---|
|pfSense|https://pfsense.mantisd.cloud|10.10.1.1|443?|
|Proxmox|https://proxmox.mantisd.cloud|10.10.1.2|8443|
| Portainer | https://portainer.mantisd.cloud| 10.10.1.3 | 9443, 9000 |
| Traefik | https://traefik-dashboard.mantisd.cloud | 10.10.1.10|443, 80, 5432:tcp,3478:tcp,10001:tcp, 8080|
|Traefik svc|https://traefik-dashboard-svc.mantisd.cloud|10.10.1.3|443, 80, 5432|
|Synology|https://syn.mantisd.cloud|10.10.1.25|5000,5001|
|Pihole - 1| https://pihole.mantisd.cloud|10.10.1.25|8888|
|Pihole - 2| https://pihole-2.mantisd.cloud|10.10.1.26|443|
|SVC| https://svc.mantisd.cloud|10.10.1.3| Portainer |
| Postgres | https://pg.svc.mantisd.cloud | 10.10.1.3 | 5432(tcp)|
|Unifi|https://unifi.mantisd.cloud|10.10.1.3| 8443 |
