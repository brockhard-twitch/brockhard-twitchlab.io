---
title: Traefik
weight: 20
---
## Traefik Configuration Details
{{< block "grid-2" >}}
{{< column >}}
**Name**
: Traefik
---
**CNAME**
: [traefik-dashboard](https://traefik-dashboard.mantisd.cloud)
---
**CertResolvers**
: Cloudflare
---
**IP**
: 10.10.1.10
---
**Providers**
: Docker & File
---
**DNS**
: 10.10.1.25
: 10.10.1.26
---
{{< /column >}}
{{< column >}}
**Name**
: Traefik-SVC
---
**CNAME**
: [traefik-dashboard-svc](https://traefik-dashboard-svc.mantisd.cloud)
---
**CertResolvers**
: n/a
---
**IP**
: 10.10.1.3
---
**Providers**
: Docker
---
**DNS**
: 10.10.1.25
: 10.10.1.26
---
{{< /column >}}
{{< /block >}}

