+++
title = "Brockhard Twitch"
[data]
baseChartOn = 3
colors = ["#627c62", "#11819b", "#ef7f1a", "#4e1154"]
columnTitles = ["Section", "Status", "Author"]
fileLink = "content/projects.csv"
title = "Projects"
+++

{{< block "grid-2" >}}
{{< column >}}

# Welcome to BrockHard Twitch Documentation Page!

I made this change.

This is the documentation page for all my home resources.  Feel free to open a [PR](https://gitlab.com/brockhard-twitch/brockhard.gitlab.io), raise an [issue](https://gitlab.com/brockhard-twitch/brockhard.gitlab.io/-/issues)(s) or request new feature(s).

{{< tip "warning" >}}
Under Construction{{< /tip >}}


{{< button "docs/" "Read the Docs" >}}
{{< /column >}}

{{< column >}}
![diy](/images/painting.jpg)
{{< /column >}}
{{< /block >}}
