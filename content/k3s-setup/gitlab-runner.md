---
title: Gitlab Runner Setup
weight: 11
---

# gitlab runner
CHANGE TO YOUR RUNNER TOKEN IN values.yaml
```bash
helm repo add gitlab https://charts.gitlab.io
```

`values.yml` is the default from
https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml

```bash
helm install --namespace default gitlab-runner -f values.yaml gitlab/gitlab-runner
```

If you need to Update the runner

```bash
helm upgrade --namespace default gitlab-runner -f values.yaml gitlab/gitlab-runner 
```


set tags
- viewscape
- k8s