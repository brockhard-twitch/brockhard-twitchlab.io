---
title: "Pihole Setup"
weight: 5
description: >
  How to setup Pihole
---

Repeat the process twice if you can 

### No Docker
In an Ubuntu VM or Container 

```
sudo su 
```

```
curl -sSL https://install.pi-hole.net | bash
```
 
just use the default settings 


change password
```
pihole -a -p 
```

http://<IP_ADDRESS_OF_YOUR_PI_HOLE>/admin/


### Docker 
```yaml
version: "3"

# https://github.com/pi-hole/docker-pi-hole/blob/master/README.md

services:
  pihole:
    container_name: pihole
    image: pihole/pihole:latest
    # For DHCP it is recommended to remove these ports and instead add: network_mode: "host"
    ports:
      - "53:53/tcp"
      - "53:53/udp"
      - "67:67/udp"
      - "80:80/tcp"
    environment:
      TZ: 'America/Phoenix'
      # WEBPASSWORD: 'set a secure password here or it will be random'
    # Volumes store your data between container upgrades
    volumes:
      - './etc-pihole:/etc/pihole'
      - './etc-dnsmasq.d:/etc/dnsmasq.d'
    #   https://github.com/pi-hole/docker-pi-hole#note-on-capabilities
    cap_add:
      - NET_ADMIN
    restart: unless-stopped # Recommended but not required (DHCP needs NET_ADMIN)  

```