---
title: K3s Setup
weight: 7
---

if you need to uninstall k3s
```bash
/usr/local/bin/k3s-uninstall.sh
```

add key to server 
```bash
scp ~/.ssh/ed_id25519.pub mantis@10.10.1.50:~/.ssh/authorized_keys
```

add key to agent 
```bash
scp ~/.ssh/ed_id25519.pub mantis@10.10.1.50:~/.ssh/authorized_keys
```

### SSH into k3s node 

Update
```bash
sudo apt-get update
sudo apt upgrade -y 
```

Install qemu agent
```shell
sudo apt-get install qemu-guest-agent      
```

Zsh (optional)
```
sudo apt-get update
sudo apt-get install zsh
sudo apt-get install powerline fonts-powerline

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```


### Fix LVM
```
sudo lvm
```

```
lvextend -l +100%FREE /dev/ubuntu-vg/ubuntu-lv
```
```
exit
```
```
sudo resize2fs /dev/ubuntu-vg/ubuntu-lv
```



## K3s Install Server 

```
curl -sfL https://get.k3s.io | sh -s - server  --disable servicelb --disable traefik --write-kubeconfig-mode 644 --kube-apiserver-arg default-not-ready-toleration-seconds=30 --kube-apiserver-arg default-unreachable-toleration-seconds=30 --kube-controller-arg node-monitor-period=20s --kube-controller-arg node-monitor-grace-period=20s --kubelet-arg node-status-update-frequency=5s 
```

### Get node Token 
Copy this down, you will need it for the agent node
```
sudo cat /var/lib/rancher/k3s/server/node-token 
```

### Copy config to home
```
cp /etc/rancher/k3s/k3s.yaml ~/
```

Exit 

## K3s Agent Server
Run All the setup commands for ubuntu

Put in your Server Node IP and Token
 ```
 curl -sfL https://get.k3s.io | K3S_TOKEN=<token> K3S_URL=https://10.10.1.50:6443 sh -s - --kubelet-arg node-status-update-frequency=5s 
```


Exit

## Get Kube Config
Now back on your computer we will get the cube config

Make sure you are in the root of the infrastructure project
Replace user@IP 
```
scp mantis@10.10.1.50:~/k3s.yaml k8s/cluster.yaml 

cp k8s/cluster.yaml ~/.kube/config

```

test with
```
kubectl get nodes
```