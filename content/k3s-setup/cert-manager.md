---
title: Cert Manager Setup
weight: 15
---

```bash
kubectl create ns cert-manager
```
CRDS
```bash
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.crds.yaml
```
```
helm repo add jetstack https://charts.jetstack.io
```
```
helm repo update
```

```bash
helm install cert-manager jetstack/cert-manager --namespace cert-manager --values=values.yaml --version v1.9.1
```

```bash
helm upgrade cert-manager jetstack/cert-manager --namespace cert-manager --values=values.yaml --version v1.9.1
```
```bash
helm uninstall cert-manager --namespace cert-manager 
```

```bash
kubectl apply -f issuers
```
```bash
kubectl apply -f certs/viewscape-staging.yaml
```