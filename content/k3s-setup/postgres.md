---
title: Postgres Setup
weight: 6
description: >
    "How to setup Postgres DB for terraform state"
---


docker-compose.yaml
```yaml
version: '3.2'

services:
  postgres:
    container_name: postgres_pi
    image: postgres
    ports:
      - 5432:5432
    volumes:
      - ./:/var/lib/postgresql
    env_file:
      - .env
    restart: always
```

.env
```
POSTGRES_PASSWORD=password
POSTGRES_USER=terraform
POSTGRES_DB=terraform_backend
```