---
title: Pipeline Setup
weight: 13
---

Now we are going to setup the piepline to automatically deploy our terraform code to our gitlab runner on our kubernetes cluster.

## Variables Setup
We will setup CICD variables for viewscape.io
https://gitlab.com/groups/viewscape-io/-/settings/ci_cd

This is where you add and edit variables for all of viewscape Group.

![src](images/k3s/tf-var.PNG)



## .gitlab-ci.yml
This will plan terraform and save it into an artifact that the apply job will apply to your cloudflare and piholes. Can also deploy to cluster
```yaml
image:
  name: registry.gitlab.com/gitlab-org/terraform-images/releases/terraform:1.1.9

stages:
  - plan
  - deploy

default:
  tags:
    - viewscape
    - k8s

variables:
  TF_ROOT: $CI_PROJECT_DIR  # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: default  # The name of the state file used by the GitLab Managed Terraform state backend
  ARTIFACT_VERSION: "1.0.0"
  REALEASE_VERSION: $ARTIFACT_VERSION
  PIHOLE_PASSWORD: $TF_VAR_PIHOLE_PASSWORD



cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

before_script:
  - rm -rf .terraform
  - terraform --version
  - terraform init


plan:
  stage: plan
  script:
    - terraform plan -out "plans"
  artifacts:
    paths:
      - plans
  tags:
    - viewscape
    - k8s

deploy:
  stage: deploy
  script:
    - terraform apply -input=false "plans"
  tags:
    - viewscape
    - k8s
  only:
    - main
  dependencies:
    - plan
```
