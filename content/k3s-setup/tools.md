---
title: Tool Install
weight: 3
---

## Kubectl

### Linux
Install kubectl binary with curl on Linux
Download the latest release with the command:
```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```

Download the kubectl checksum file:
```
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
```

Validate the kubectl binary against the checksum file:
```
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
```

If valid, the output is:
```
kubectl: OK
```

If the check fails, sha256 exits with nonzero status and prints output similar to:
```
kubectl: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
```
Note: Download the same version of the binary and checksum.
Install kubectl

```
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

Note:
If you do not have root access on the target system, you can still install kubectl to the ~/.local/bin directory:

```
chmod +x kubectl
mkdir -p ~/.local/bin
mv ./kubectl ~/.local/bin/kubectl
```
and then append (or prepend) ~/.local/bin to $PATH

Test to ensure the version you installed is up-to-date:
```
kubectl version --client
```
### Mac
```
brew install kubectl
```

or...

1. Download
```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/amd64/kubectl"
```

2. Make Executable
```
chmod +x ./kubectl
```

3. Move the kubectl binary to a file location on your system PATH.
```
sudo mv ./kubectl /usr/local/bin/kubectl
sudo chown root: /usr/local/bin/kubectl
```

## Helm
```
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

## Terraform
### linux

```
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list

sudo apt update && sudo apt install terraform
```

### Mac
```
brew tap hashicorp/tap

brew install hashicorp/tap/terraform
```
