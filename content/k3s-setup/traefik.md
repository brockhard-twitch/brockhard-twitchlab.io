---
title: "Traefik Setup"
weight: 9
description: >
  How to setup Traefik 
---

## Traefik

Add repo
```shell
helm repo add traefik https://helm.traefik.io/traefik
```

Update repo
```shell
helm repo update
```
Create our namespace
```shell
kubectl create namespace traefik
```
Get all namespaces
```shell
kubectl get namespaces
```
We should see
```
NAME              STATUS   AGE
default           Active   21h
kube-node-lease   Active   21h
kube-public       Active   21h
kube-system       Active   21h
metallb-system    Active   21h
traefik           Active   12s
```
traefik values.yaml
```yaml
globalArguments:
  - "--global.sendanonymoususage=false"
  - "--global.checknewversion=false"

additionalArguments:
  - "--serversTransport.insecureSkipVerify=true"
  - "--log.level=INFO"

deployment:
  enabled: true
  replicas: 3
  annotations: {}
  podAnnotations: {}
  additionalContainers: []
  initContainers: []

ports:
  web:
    redirectTo: websecure
  websecure:
    tls:
      enabled: true
      
ingressRoute:
  dashboard:
    enabled: false

providers:
  kubernetesCRD:
    enabled: true
    ingressClass: traefik-external
  kubernetesIngress:
    enabled: true
    publishedService:
      enabled: false

rbac:
  enabled: true

service:
  enabled: true
  type: LoadBalancer
  annotations: {}
  labels: {}
  spec:
    loadBalancerIP: 10.10.1.60 # this should be an IP in the MetalLB range
  loadBalancerSourceRanges: []
  externalIPs: []
```

Install traefik
```shell
helm install --namespace=traefik traefik traefik/traefik --values=values.yaml
```
Check the status of the traefik ingress controller service
```shell
kubectl get svc --all-namespaces -o wide
```
We should see traefik with the specified IP
```
NAMESPACE        NAME              TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE   SELECTOR
          443/TCP                      16h   <none>
          53/UDP,53/TCP,9153/TCP       16h   k8s-app=kube-dns
          443/TCP                      16h   k8s-app=metrics-server
          443/TCP                      16h   component=controller
traefik          traefik           LoadBalancer   10.43.156.161   192.168.30.80   80:30358/TCP,443:31265/TCP   22s   app.kubernetes.io/instance=traefik,app.kubernetes.io/name=traefik
```
Ger all pods in traefik namespace
```shell
kubectl get pods --namespace traefik
```
We should see pods in the traefik namespace
```
NAME                       READY   STATUS    RESTARTS   AGE
traefik-76474c4d47-l5z74   1/1     Running   0          11m
traefik-76474c4d47-xb282   1/1     Running   0          11m
traefik-76474c4d47-xx5lw   1/1     Running   0          11m
```
### middleware
Apply middleware
```shell
kubectl apply -f default-headers.yaml
```
Get middleware
```shell
kubectl get middleware
```
We should see our headers
```
NAME              AGE
default-headers   25s
dashboard
```
### Install htpassword
```shell
sudo apt-get update
sudo apt-get install apache2-utils
```
Generate a credential / password that’s base64 encoded
```shell
htpasswd -nb username password | openssl base64
```
secret-dashboard.yaml
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: traefik-dashboard-auth
  namespace: traefik
type: Opaque
data:
  users: <FROM HTPASSWORD>
```

Apply secret
```shell
kubectl apply -f secret-dashboard.yaml
```
Get secret
```shell
kubectl get secrets --namespace traefik
```

dashboard-auth-middleware.yaml
```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: traefik-dashboard-basicauth
  namespace: traefik
spec:
  basicAuth:
    secret: traefik-dashboard-auth
```

Apply middleware
```shell
kubectl apply -f dashboard-auth-middleware.yaml
```
ingress.yaml
```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: traefik-dashboard
  namespace: traefik
  annotations: 
    kubernetes.io/ingress.class: traefik-external
spec:
  entryPoints:
    - websecure
  routes:
    - match: Host(`traefik.viewscape.io`)
      kind: Rule
      middlewares:
        - name: traefik-dashboard-basicauth
          namespace: traefik
      services:
        - name: api@internal
          kind: TraefikService
  tls:
    secretName: viewscape-io-staging-tls
```
Apply dashboard
```shell
kubectl apply -f ingress.yaml
```
Visit https://traefik.viewscape.io

## Sample Workload
deployment.yaml
```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: nginx-test-deployment
  namespace: default
  labels:
    app: test
spec:
  replicas: 3
  progressDeadlineSeconds: 600
  revisionHistoryLimit: 2
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: test
  template:
    metadata:
      labels:
        app: test
    spec:
      containers:
      - name: nginx-test
        image: nginx
```

service.yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-test-service
  namespace: default
spec:
  selector:
    app: test
  ports:
  - name: http
    targetPort: 80
    port: 80
```

ingress.yaml
```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: nginx-test-ingress
  namespace: default
  annotations: 
    kubernetes.io/ingress.class: traefik-external
spec:
  entryPoints:
    - websecure
  routes:
    - match: Host(`viewscape.io`)
      kind: Rule
      services:
        - name: test
          port: 80
      middlewares:
        - name: default-headers
  tls:
    secretName: viewscape-io-tls
```

```shell
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
kubectl apply -f ingress.yaml
```

make sure you have a DNS record
got to https://viewscape.io
