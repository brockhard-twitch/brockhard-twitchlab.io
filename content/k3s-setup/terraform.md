---
title: Terraform Setup
weight: 12
---

To make the terraform setup easy i configured all the routes you will need. to add more go into the module and copy the format

The locals.tf file needs to be configured with the following

Change to you pihole ips and correct email and the domain ip is the one you gave traefik. mine is 10.10.1.60

```tf
locals {
  pihole_primary_url   = "http://10.10.1.25:8888" # IF you have it on a different port
  pihole_secondary_url = "http://10.10.1.26" # If you didnt change the port setting
  cloudflare_email     = "@gmail.com"
  domain_name          = "viewscape.io"
  domain_ip            = "x.x.x.x"
}

```

If not using postgres as a backend then go into variables.tf and set the defaults for
If you are using postgres we will set these in the pipeline
```tf
variable "CLOUDFLARE_API_KEY" {
  type      = string
  sensitive = true
  default = ""
}

variable "PIHOLE_PASSWORD" {
  type      = string
  sensitive = true
  default = ""
}

variable "CLOUDFLARE_ZONE_ID" {
  type      = string
  sensitive = true
  default = ""
}

```



