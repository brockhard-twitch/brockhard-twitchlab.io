---
title: "MetalLb"
weight: 8
description: >
  How to setup Metal LB
---

Install by manifest

```shell
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.4/config/manifests/metallb-native.yaml
```

Configure
```yaml
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: address-pool
  namespace: metallb-system
spec:
  addresses:
  - 10.10.1.60-10.10.1.100
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: address-advertisement
  namespace: metallb-system

```
