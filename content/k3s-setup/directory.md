---
title: Directory
weight: 2
---

| Name | Description | IP | Port | 
| --- | --- | --- | --- | 
| Pihole 1 | DNS Server | 10.10.1.25 | 8888 |  
| Pihole 2 | DNS Server | 10.10.1.26 | 80 |
| Proxmox | Hypervisor | 10.10.1.3| _ |
| Postgres | Database for Terraform | 10.10.1.30 | 5432 |
| Server Node | K3S Master Node | 10.10.1.50 | 6443 |
| Agent Node | K3S Agent Node | 10.10.1.40 | _ | 
 

