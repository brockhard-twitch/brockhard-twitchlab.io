---
title: Longhorn Setup
weight: 10
---
## Longhorn Helm Install
Add the Longhorn Helm repository:
```shell
helm repo add longhorn https://charts.longhorn.io
```
Fetch the latest charts from the repository:
```shell
helm repo update
```
Install Longhorn in the longhorn-system namespace.

To install Longhorn with Helm 3, use the commands:
```shell
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace
```
To confirm that the deployment succeeded, run:
```shell
kubectl -n longhorn-system get pod
```

## Install UI
---
```shell
kubectl apply -f k8s/longhorn 
```
https://longhorn.viewscape.io

## CHANGE AUTH OR PASSWORD IS SAME AS traefik
brock B@ckspace1