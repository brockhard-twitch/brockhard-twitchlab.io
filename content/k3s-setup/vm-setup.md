---
title: VM Setup
weight: 4
description: >
  How to setup Ubuntu VM in Proxmox
---

In proxmox click on create VM.

Name the VM k3s-server-node-0 and give it an ID of 200 

![src](/images/k3s/VM-name.PNG)


### ISO 
Pick ubuntu-20.04.4-live-server-amd64.iso

![src](/images/k3s/vm-iso.PNG)

### Qemu 
Enable the Qemu Agent, we will install it on ubuntu later

![src](/images/k3s/qemu-agent.PNG)

### Disks
Pick the  local-lvm and give it 200GB of disk space. You can also use any disk size or location you want to. I would try to use more than 80Gb each

![src](/images/k3s/vm-disk.PNG)

### CPU
Give it 4 Cores or as many as you want 

![src](/images/k3s/vm-cpu.PNG)

### Memory
I gave mine 8GB of RAM which is 8192 

![src](/images/k3s/vm-memory.PNG)

### Network
Select your Correct Network 

![src](/images/k3s/vm-network.PNG)

### Agent
Now Confirm and finish then Repeat all the Steps for the agent node but give it 

Name
: k3s-agent-0

ID
: 300
---

<br/>

## Install Ubuntu 
---
Click on k3s-server-0 and start the VM (top right of page)

Click on the console tab to VNC into the VM 

Select English

### Network
Switch the interface to manual ipv4
Give it your IP address, for mine I am using 10.10.1.50 for server and 10.10.1.40 for agent
{{< tip title="warning"  >}}
  Put In the IP address of your pihole for the name servers. 
{{< /tip >}}
![src](/images/k3s/ubuntu-network.PNG)


### Profile
Select all the defaults up to the profile setup 

Give it the NAme and password you want to use, It is only important that you give the server name the correct name. Important for the nodes.

![src](/images/k3s/ubuntu-profile.PNG)


### Last Part
Enable ssh and skip any other packages. It will now install

Repeat the steps for k3s-agent-0 and give it

server name
: k3s-agent-0

IP (Yours, this is what i used)
: 10.10.1.40