---
title: "K3S Setup Guide"
weight: 1
---

My guide to setting up K3S on Proxmox Ubuntu VMs

**Requirments**
---
- Proxmox
- 2 Ubuntu VMS
- Pihole (Preferable 2)
- Postgres Db
- terraform
- kubectl 
- helm 

---



{{< button "./directory" "Directory" >}}

{{< button "../docs/" "Back to docs" >}}

 