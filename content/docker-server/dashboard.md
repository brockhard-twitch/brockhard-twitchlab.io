---
title: Dashboard
weight: 20
---

[Heimdall Dashboard](https://hub.docker.com/r/linuxserver/heimdall)

|Field|Info|
|---|---|
| IP | 10.10.1.30 |
| Port | 443, 80 |
| URL | mantisd.io | 
| URL | [https://${SERVER_IP}:443](https://10.10.1.30:443)|
| DIR | ~/dashboard |


## Docker Compose

```yaml
---
version: "2.1"

volumes:
  dashboard:

services:
  dashboard:
    image: lscr.io/linuxserver/heimdall:latest
    container_name: dashboard
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Phoenix
    volumes:
      - dashboard:/config
    networks:
      - proxy
    ports:
      - 80:80
      - 443:443
    restart: unless-stopped
networks:
  proxy:
    external: true
```

## Init

```bash
sudo docker-compose up -d
```


### IF UPDATING
```bash
sudo docker-compose up -d --force-recreate
```

### Destroy
```bash
sudo docker-compose down
```
