---
title: Server Setup
weight: 12
---
## SSH
### Generate ssh key
```bash
ssh-keygen -t ed25519 -C "name key"
# Where do you want to save and password
eval $(ssh-agent -s)
ssh-add ~/.ssh/id_ed25519
cat ~/.ssh/id_ed25519.pub
```

### Copy ssh key to server
```bash
scp ~/.ssh/id_ed25519.pub ${USERNAME}@${SERVER_IP}:~/.ssh/authorized_keys
```

### SSH IN
```bash 
ssh ${USERNAME}@${SERVER_IP}
```
## Create Docker Network

Create a Docker network for all the container to run on. This will connect later to Traefik Reverse Proxy and we wont have to remember IP addresses and ports!


```bash
docker network create proxy
```

## Create Portainer DIR
```bash
cd ~/
mkdir ~/portainer
```


## SCP Copy docker-compose.yml to Server

Change directory into folder containing docker-compose.yml

```bash
scp docker-compose.yml ${username}@${server_ip}:~/portainer/
```

