---
title: Gitlab Runner Setup
weight: 15
---

The gitlab runner will run our pipeline jobs


## Docker Compose

```yaml
version: '3.2'

volumes:
  mantisd_config:

services:
  gitlab-runner:
    container_name: gitlab-runner
    image: gitlab/gitlab-runner:latest
    volumes:
      - "mantisd_config:/etc/gitlab-runner"
      - "/var/run/docker.sock:/var/run/docker.sock"
    restart: unless-stopped
    command: 
    - register
    - --non-interactive
    - --url "https://gitlab.com/" 
    - --registration-token "${REG_TOKEN}" 
    - --executor "docker"
    - --description "docker-runner" 
    - --docker-image alpine:latest
    - --maintenance-note "Version 1.0.0" 
    - --tag-list "brockhard twitch" 
    - --run-untagged="false" 

```

## SCP to server in ~/runner

Copy docker compose file into server

Create a folder called data


## Register Runner
```bash
docker run \
   --name runner \
   -v ${PWD}/data/:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   gitlab/gitlab-runner:latest \ 
   register
```
## Run Runner
```bash
docker run \
  -d --name runner \
  -v ${PWD}/data/:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \ 
  gitlab/gitlab-runner:latest
```
## Remove Runner
```bash
docker rm runner
```
> You Will have to re Register!


