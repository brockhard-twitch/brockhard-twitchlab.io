---
title: Docker Install
weight: 10
---

## Install Docker

To install Docker run this command in an Ubuntu server

```bash
curl -sSL https://get.docker.com | bash
sudo usermod -aG docker $(whoami)
sudo systemctl enable docker.service
sudo systemctl start docker.service
```
> May or May not need sudo for systemctl

## Install docker-compose

```bash
sudo apt-get -y install docker-compose
```


## test

```bash
docker version
```