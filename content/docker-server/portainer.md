---
title: Portainer
weight: 10
---

## Portainer
Portainer is our Docker Server

|Field|Info|
|---|---|
| IP | 10.10.1.30 |
| Port | 9443 |
| URL | docker-server.mantisd.io | 
| URL | [https://${SERVER_IP}:9443](https://10.10.1.30:9443)|
| DIR | ~/portainer |

## Docker Compose

```yaml
version: '3'

volumes:
  portainer_data:

services:
  portainer:
    image: portainer/portainer-ce:latest
    ports:
      - 9000:9000
      - 9443:9443
    container_name: portainer
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data
      - /etc/localtime:/etc/localtime:ro
    dns:
      - 10.10.1.25
      - 10.10.1.26

networks:
  default:
    external:
      name: proxy
```


