---
title: Deploy Server
weight: 10
---


## Deploy
ssh into server

Go into Portainer Directory

```bash
cd ~/portainer
```

## Docker Compose
run docker compose up

```bash
sudo docker-compose up -d
```


### IF UPDATING
```bash
sudo docker-compose up -d --force-recreate
```

### Destroy
```bash
sudo docker-compose down
```


# Visit in Browser

https://${SERVER_IP}:9443

https://10.10.1.30:9443